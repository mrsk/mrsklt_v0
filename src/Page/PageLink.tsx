
import * as React from 'react';
import "./PageLink.css";

interface IPageLinkProps {
    onClick: any,
    onClickArgs: any,

}

class PageLink extends React.Component<IPageLinkProps> {
    constructor(args: any) {
        super(args);
        this.getOnClick = this.getOnClick.bind(this);
    }

    public render() {
        return <div 
            className="PageLink"
            onClick={this.getOnClick()}
        >
            {this.props.children}
        </div>
    }

    private getOnClick() {
        return () => this.props.onClick(this.props.onClickArgs);
    }
}

export default PageLink;
