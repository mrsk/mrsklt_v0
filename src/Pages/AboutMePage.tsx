
import * as React from 'react';
import PageContent from "src/Page/PageContent";
import PageLink from "src/Page/PageLink";
import PageTitle from "src/Page/PageTitle";

import ProfileImage from "./ProfileImage.png";

class AboutMePage extends React.Component {



    public static renderLink(key: number, onSelect: any): JSX.Element {
        
        return <PageLink 
            key={key}
            onClickArgs="#AboutMe"
            onClick={onSelect}
        >
            About me
        </PageLink>
        
        
    }

    public render() {
        return <div>
            {this.renderTitle()}
            {this.renderContent()}
        </div>
    }


    public renderTitle() {
        return <PageTitle>
            About me
        </PageTitle>
    }


    public renderContent() {
        return <PageContent>
            <img 
                style={{
                    maxWidth: "100%"
                }}
                src={ProfileImage} 
            />
            
            <br/>
            <br/>
            
            Hi, my name is Marius. I am a dreamer / computer scientist / physicist / nature lover.
            I can help you solve or avoid tech problems.

            <br/>
            <br/>
            
            A few things I like doing:
            <ul style={{ "marginTop": 0 }}>
                <li>Cycling</li>
                <li>Folk dancing</li>
                <li>Sailing</li>
                <li>Stargazing</li>
                <li>Climbing mountains</li>
            </ul>

            You can contact me on <a href="https://riot.im/app/#/user/@marius_k:matrix.org">matrix.org</a> network.
            
            <br/>
            <br/>
            
            <a href="CurriculumVitae.txt" target="_blank">preview cv</a>
            <br/>
            <a href="CurriculumVitae.txt" target="_blank" download="cvMariusKavaliauskas.txt">download cv</a>
            
            
        </PageContent>
    }


}

export default AboutMePage;
