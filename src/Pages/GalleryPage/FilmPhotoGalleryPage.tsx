

import * as React from "react";
import NginxFGalleryPage from "./NginxFGalleryPage";
import HashRouteController from "src/HashRouteController";

interface IFilmPhotoGalleryPageProps {
    unparsedRoute: string[],
}
class FilmPhotoGalleryPage extends React.Component<IFilmPhotoGalleryPageProps> {
    

    public render() {
        
        return <NginxFGalleryPage
            fGalleryUrl="https://ovh.mrsk.lt/pics/filmphoto/"
            title="Film photography"
            unparsedRoute={this.props.unparsedRoute}
            // tslint:disable-next-line:jsx-no-lambda
            setRoute={(getRoute) => {
                const baseRoute = "#Gallery/FilmPhoto";
                const route = getRoute(baseRoute);
                HashRouteController.setHashRoute(route);
            }}
        />

    }
    
}

export default FilmPhotoGalleryPage;
