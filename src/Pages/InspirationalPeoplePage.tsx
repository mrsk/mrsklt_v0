
import * as React from 'react';
import PageContent from "src/Page/PageContent";
import PageLink from "src/Page/PageLink";
import PageTitle from "src/Page/PageTitle";


class InspirationalPeoplePage extends React.Component {
    
    public static renderLink(key: number, onSelect: any): JSX.Element {
        
        return <PageLink 
            key={key}
            onClickArgs="#InspirationalPeople"
            onClick={onSelect}
        >
            Inspirational people
        </PageLink>
        
        
    }

    constructor(args: any) {
        super(args);

        this.renderTitle = this.renderTitle.bind(this);
        this.renderContent = this.renderContent.bind(this);
    }

    public render() {
        return <div>
            {this.renderTitle()}
            {this.renderContent()}
        </div>
    }


    public renderTitle() {
        return <PageTitle>
            Inspirational people
        </PageTitle>
    }


    public renderContent() {
        return <PageContent>
            <ul>
                <li>Carl Sagan</li>
                <li>Linus Torvalds</li>
                <li>Roger Penrose</li>
                <li>Vi Hart</li>
                <li>Marshall Rosenberg</li>
                <li>Louis Sauzedde</li>
                <li>Elon Musk</li>
                <li>Richard Stallman</li>
                <li>Lucius Annaeus Seneca</li>
                <li>Maurits Cornelis Escher</li>
            </ul>
            I hope they don't mind beeing together on one list:)
        </PageContent>
    }

}

export default InspirationalPeoplePage;
