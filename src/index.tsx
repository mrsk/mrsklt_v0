import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import HashRouteController from "./HashRouteController"
import './index.css';


HashRouteController.init();

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
